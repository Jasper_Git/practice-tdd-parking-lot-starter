package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @program: practice-tdd-parking-lot-starter
 * @author: yoki
 * @create: 2023-07-16 14:22
 */
public class ParkingLotManagerTest {

    @Test
    void should_return_a_ticket_when_the_parking_boy_specified_by_manager_park_given_a_car() {
        // given
        List<ParkingLot> parkingLots1 = Arrays.asList(new ParkingLot(), new ParkingLot());
        List<ParkingLot> parkingLots2 = Arrays.asList(new ParkingLot(), new ParkingLot());
        List<ParkingLot> parkingLots3 = Arrays.asList(new ParkingLot(), new ParkingLot());
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots1);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots3);
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        parkingLotManager.addParkingBoy(parkingBoy);
        parkingLotManager.addParkingBoy(smartParkingBoy);
        parkingLotManager.addParkingBoy(superSmartParkingBoy);
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();

        // when
        Ticket ticket1 = parkingLotManager.specifyToPark(parkingBoy, car1);
        Ticket ticket2 = parkingLotManager.specifyToPark(smartParkingBoy, car2);
        Ticket ticket3 = parkingLotManager.specifyToPark(superSmartParkingBoy, car3);

        // then
        Assertions.assertEquals(car1, parkingBoy.fetch(ticket1));
        Assertions.assertEquals(car2, smartParkingBoy.fetch(ticket2));
        Assertions.assertEquals(car3, superSmartParkingBoy.fetch(ticket3));
    }

    @Test
    void should_return_a_car_when_the_parking_boy_specified_by_manager_fetch_given_a_ticket() {
        // given
        List<ParkingLot> parkingLots1 = Arrays.asList(new ParkingLot(), new ParkingLot());
        List<ParkingLot> parkingLots2 = Arrays.asList(new ParkingLot(), new ParkingLot());
        List<ParkingLot> parkingLots3 = Arrays.asList(new ParkingLot(), new ParkingLot());
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots1);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots3);
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        parkingLotManager.addParkingBoy(parkingBoy);
        parkingLotManager.addParkingBoy(smartParkingBoy);
        parkingLotManager.addParkingBoy(superSmartParkingBoy);
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        Ticket ticket1 = parkingLotManager.specifyToPark(parkingBoy, car1);
        Ticket ticket2 = parkingLotManager.specifyToPark(smartParkingBoy, car2);
        Ticket ticket3 = parkingLotManager.specifyToPark(superSmartParkingBoy, car3);

        // when
        Car fetchCar1 = parkingLotManager.specifyToFetch(parkingBoy, ticket1);
        Car fetchCar2 = parkingLotManager.specifyToFetch(smartParkingBoy, ticket2);
        Car fetchCar3 = parkingLotManager.specifyToFetch(superSmartParkingBoy, ticket3);

        // then
        Assertions.assertEquals(car1, fetchCar1);
        Assertions.assertEquals(car2, fetchCar2);
        Assertions.assertEquals(car3, fetchCar3);
    }

    @Test
    void should_return_error_when_the_parking_boy_specified_by_manager_fetch_given_a_unrecognized_ticket() {
        // given
        List<ParkingLot> parkingLots1 = Arrays.asList(new ParkingLot(), new ParkingLot());
        List<ParkingLot> parkingLots2 = Arrays.asList(new ParkingLot(), new ParkingLot());
        List<ParkingLot> parkingLots3 = Arrays.asList(new ParkingLot(), new ParkingLot());
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots1);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots3);
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        parkingLotManager.addParkingBoy(parkingBoy);
        parkingLotManager.addParkingBoy(smartParkingBoy);
        parkingLotManager.addParkingBoy(superSmartParkingBoy);
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        parkingLotManager.specifyToPark(parkingBoy, car1);
        parkingLotManager.specifyToPark(smartParkingBoy, car2);
        parkingLotManager.specifyToPark(superSmartParkingBoy, car3);
        Ticket ticket = new Ticket();

        // when

        // then
        UnrecognizedTicketException unrecognizedTicketException1 =
                Assertions.assertThrows(UnrecognizedTicketException.class, () -> parkingLotManager.specifyToFetch(parkingBoy, ticket));
        assertEquals("Unrecognized parking ticket", unrecognizedTicketException1.getMessage());
        UnrecognizedTicketException unrecognizedTicketException2 =
                Assertions.assertThrows(UnrecognizedTicketException.class, () -> parkingLotManager.specifyToFetch(smartParkingBoy, ticket));
        assertEquals("Unrecognized parking ticket", unrecognizedTicketException2.getMessage());
        UnrecognizedTicketException unrecognizedTicketException3 =
                Assertions.assertThrows(UnrecognizedTicketException.class, () -> parkingLotManager.specifyToFetch(superSmartParkingBoy, ticket));
        assertEquals("Unrecognized parking ticket", unrecognizedTicketException3.getMessage());
    }

    @Test
    void should_return_error_when_the_parking_boy_specified_by_manager_fetch_given_a_used_ticket() {
        // given
        List<ParkingLot> parkingLots1 = Arrays.asList(new ParkingLot(), new ParkingLot());
        List<ParkingLot> parkingLots2 = Arrays.asList(new ParkingLot(), new ParkingLot());
        List<ParkingLot> parkingLots3 = Arrays.asList(new ParkingLot(), new ParkingLot());
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots1);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots3);
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        parkingLotManager.addParkingBoy(parkingBoy);
        parkingLotManager.addParkingBoy(smartParkingBoy);
        parkingLotManager.addParkingBoy(superSmartParkingBoy);
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        Ticket ticket1 = parkingLotManager.specifyToPark(parkingBoy, car1);
        Ticket ticket2 = parkingLotManager.specifyToPark(smartParkingBoy, car2);
        Ticket ticket3 = parkingLotManager.specifyToPark(superSmartParkingBoy, car3);
        parkingLotManager.specifyToFetch(parkingBoy, ticket1);
        parkingLotManager.specifyToFetch(smartParkingBoy, ticket2);
        parkingLotManager.specifyToFetch(superSmartParkingBoy, ticket3);

        // when

        // then
        UnrecognizedTicketException unrecognizedTicketException1 =
                Assertions.assertThrows(UnrecognizedTicketException.class, () -> parkingLotManager.specifyToFetch(parkingBoy, ticket1));
        assertEquals("Unrecognized parking ticket", unrecognizedTicketException1.getMessage());
        UnrecognizedTicketException unrecognizedTicketException2 =
                Assertions.assertThrows(UnrecognizedTicketException.class, () -> parkingLotManager.specifyToFetch(smartParkingBoy, ticket2));
        assertEquals("Unrecognized parking ticket", unrecognizedTicketException2.getMessage());
        UnrecognizedTicketException unrecognizedTicketException3 =
                Assertions.assertThrows(UnrecognizedTicketException.class, () -> parkingLotManager.specifyToFetch(superSmartParkingBoy, ticket3));
        assertEquals("Unrecognized parking ticket", unrecognizedTicketException3.getMessage());
    }

    @Test
    void should_return_error_when_the_parking_boy_specified_by_manager_park_given_all_full_parkingLots() {
        // given
        List<ParkingLot> parkingLots1 = Arrays.asList(new ParkingLot(1), new ParkingLot(1));
        List<ParkingLot> parkingLots2 = Arrays.asList(new ParkingLot(1), new ParkingLot(2));
        List<ParkingLot> parkingLots3 = Arrays.asList(new ParkingLot(2), new ParkingLot(1));
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots1);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots3);
        ParkingLotManager parkingLotManager = new ParkingLotManager();
        parkingLotManager.addParkingBoy(parkingBoy);
        parkingLotManager.addParkingBoy(smartParkingBoy);
        parkingLotManager.addParkingBoy(superSmartParkingBoy);
        parkingLotManager.specifyToPark(parkingBoy, new Car());
        parkingLotManager.specifyToPark(parkingBoy, new Car());
        parkingLotManager.specifyToPark(smartParkingBoy, new Car());
        parkingLotManager.specifyToPark(smartParkingBoy, new Car());
        parkingLotManager.specifyToPark(smartParkingBoy, new Car());
        parkingLotManager.specifyToPark(superSmartParkingBoy, new Car());
        parkingLotManager.specifyToPark(superSmartParkingBoy, new Car());
        parkingLotManager.specifyToPark(superSmartParkingBoy, new Car());
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();

        // when

        // then
        NoPositionException noPositionException1 = Assertions.assertThrows(NoPositionException.class, () -> parkingLotManager.specifyToPark(parkingBoy, car1));
        assertEquals("No available position", noPositionException1.getMessage());
        NoPositionException noPositionException2 = Assertions.assertThrows(NoPositionException.class, () -> parkingLotManager.specifyToPark(smartParkingBoy, car2));
        assertEquals("No available position", noPositionException1.getMessage());
        NoPositionException noPositionException3 = Assertions.assertThrows(NoPositionException.class, () -> parkingLotManager.specifyToPark(superSmartParkingBoy, car3));
        assertEquals("No available position", noPositionException1.getMessage());
    }

    @Test
    void should_return_a_ticket_when_manager_park_as_parkingBoy_given_a_car_and_parkingLots_managed_by_manager() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(2);
        List<ParkingLot> parkingLots = Arrays.asList(parkingLot1, parkingLot2);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLots);

        // when
        Ticket ticket = parkingLotManager.park(new Car());

        // then
        boolean containTicket = parkingLot2.getTicketCarMap().containsKey(ticket);
        assertTrue(containTicket);
    }

    @Test
    void should_return_a_car_when_manager_park_as_parkingBoy_given_a_ticket_and_parkingLots_managed_by_manager() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(2);
        List<ParkingLot> parkingLots = Arrays.asList(parkingLot1, parkingLot2);
        ParkingLotManager parkingLotManager = new ParkingLotManager(parkingLots);
        Car car = new Car();
        Ticket ticket = parkingLotManager.park(car);

        // when
        Car fetchCar = parkingLotManager.fetch(ticket);

        // then
        assertEquals(car, fetchCar);
    }
}
