package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @program: practice-tdd-parking-lot-starter
 * @author: yoki
 * @create: 2023-07-16 02:15
 */
public class SuperSmartParingBoyTest {
    @Test
    void should_park_car_to_the_parkingLot_with_the_larger_available_position_rate_when_given_two_parkingLots_and_a_car() {
        // given
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(3);
        ParkingLot parkingLot2 = new ParkingLot(4);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        superSmartParkingBoy.park(car1);
        superSmartParkingBoy.park(car3);
        superSmartParkingBoy.park(car2);
        Car car4 = new Car();

        // when
        Ticket ticket = superSmartParkingBoy.park(car4);

        // then
        Car car = parkingLot1.fetch(ticket);
        Assertions.assertEquals(car4, car);
    }

    @Test
    void should_return_the_parked_car_when_fetch_given_multiple_parkingLots_with_a_parked_car_and_a_ticket() {
        // given
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(20);
        parkingLots.add(parkingLot2);
        parkingLots.add(parkingLot1);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);
        Car car = new Car();
        Ticket ticket = superSmartParkingBoy.park(car);

        // when
        Car car1 = superSmartParkingBoy.fetch(ticket);

        // then
        Assertions.assertEquals(car, car1);
    }


    @Test
    public void should_return_right_car_with_each_ticket_when_fetch_twice_given_two_parkingLot_both_with_a_parked_car_and_two_ticket() {
        //given
        ArrayList<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        Car car1 = new Car();
        Car car2 = new Car();
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);
        Ticket ticket1 = superSmartParkingBoy.park(car1);
        Ticket ticket2 = superSmartParkingBoy.park(car2);

        //when
        Car fetchCar1 = parkingLot1.fetch(ticket1);
        Car fetchCar2 = parkingLot2.fetch(ticket2);

        //then
        assertEquals(car1, fetchCar1);
        assertEquals(car2, fetchCar2);
    }

    @Test
    public void should_return_error_when_fetch_given_an_recognized_ticket() {
        //given
        ArrayList<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        Car car1 = new Car();
        Car car2 = new Car();
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);
        superSmartParkingBoy.park(car1);
        superSmartParkingBoy.park(car2);

        //when

        //then
        UnrecognizedTicketException unrecognizedTicketException =
                assertThrows(UnrecognizedTicketException.class, () -> superSmartParkingBoy.fetch(new Ticket()));
        assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    public void should_return_error_when_fetch_given_a_used_ticket() {
        //given
        Car car = new Car();
        ArrayList<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot();

        ParkingLot parkingLot2 = new ParkingLot();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);
        Ticket ticket = superSmartParkingBoy.park(car);
        superSmartParkingBoy.fetch(ticket);

        //when

        //then
        UnrecognizedTicketException unrecognizedTicketException = assertThrows(UnrecognizedTicketException.class, () -> superSmartParkingBoy.fetch(ticket));
        assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }


    @Test
    public void should_return_error_when_two_parking_lot_both_without_position() {
        //given
        ArrayList<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(2);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        superSmartParkingBoy.park(car1);
        superSmartParkingBoy.park(car2);
        superSmartParkingBoy.park(car3);
        Car car = new Car();

        //when

        //then
        NoPositionException noPositionException = assertThrows(NoPositionException.class, () -> superSmartParkingBoy.park(car));
        assertEquals("No available position", noPositionException.getMessage());
    }
}
