package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: practice-tdd-parking-lot-starter
 * @author: yoki
 * @create: 2023-07-16 14:37
 */
public class ParkingLotManager extends ParkingBoy {
    private List<Boy> parkingBoys;

    public ParkingLotManager() {
        this.parkingBoys = new ArrayList<>();
    }

    public ParkingLotManager(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    public void addParkingBoy(Boy parkingBoy) {
        if (parkingBoys.contains(parkingBoy)) {
            return;
        }
        parkingBoys.add(parkingBoy);
    }

    public Ticket specifyToPark(Boy parkingBoy, Car car) {
        if (parkingBoys.contains(parkingBoy)) {
            return parkingBoy.park(car);
        }
        throw new NoPositionException();
    }

    public Car specifyToFetch(Boy parkingBoy, Ticket ticket) {
        if (parkingBoys.contains(parkingBoy)) {
            return parkingBoy.fetch(ticket);
        }
        throw new UnrecognizedTicketException();
    }
}
