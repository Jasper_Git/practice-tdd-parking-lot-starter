package com.parkinglot;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {
    private Map<Ticket, Car> ticketCarMap = new HashMap<>();

    private final int capacity;

    public ParkingLot() {
        this.capacity = 10;
    }

    public ParkingLot(int capacity) {
        this.capacity = capacity;
    }

    public Ticket park(Car car) {
        if (isNotFull()) {
            Ticket ticket = new Ticket();
            this.ticketCarMap.put(ticket, car);
            return ticket;
        }
        throw new NoPositionException();
    }

    public boolean isNotFull() {
        return ticketCarMap.size() < capacity;
    }

    public Car fetch(Ticket ticket) {
        if (ticket == null) {
            throw new UnrecognizedTicketException();
        }
        if (!ticketCarMap.containsKey(ticket) || ticket.isUsed()) {
            throw new UnrecognizedTicketException();
        }

        Car car = ticketCarMap.get(ticket);
        ticketCarMap.remove(ticket);
        return car;
    }

    public Map<Ticket, Car> getTicketCarMap() {
        return ticketCarMap;
    }

    public int getAvailableCapacity() {
        return this.capacity - this.ticketCarMap.size();
    }

    public float getAvailablePositionRate() {
        return (float) (this.capacity - this.ticketCarMap.size()) / (float) this.capacity;
    }
}
