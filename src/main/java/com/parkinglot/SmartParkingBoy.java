package com.parkinglot;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @program: practice-tdd-parking-lot-starter
 * @author: yoki
 * @create: 2023-07-15 19:20
 */
public class SmartParkingBoy extends Boy{

    public SmartParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    @Override
    public Ticket park(Car car) {
        Optional<ParkingLot> min = parkingLots.stream().filter(ParkingLot::isNotFull).
                min((parkingLot1, parkingLot2) -> parkingLot2.getAvailableCapacity() - parkingLot1.getAvailableCapacity());
        if (min.isPresent()) {
            return min.get().park(car);
        }
        throw new NoPositionException();
    }

    @Override
    public Car fetch(Ticket ticket) {
        Optional<ParkingLot> parkingLotOptional = parkingLots.stream().filter(parkingLot -> parkingLot.getTicketCarMap().containsKey(ticket)).findAny();
        if (parkingLotOptional.isPresent()) {
            return parkingLotOptional.get().fetch(ticket);
        }
        throw new UnrecognizedTicketException();
    }
}
