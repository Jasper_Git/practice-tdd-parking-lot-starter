package com.parkinglot;

import java.util.List;
import java.util.Optional;

/**
 * @program: practice-tdd-parking-lot-starter
 * @author: yoki
 * @create: 2023-07-16 02:15
 */
public class SuperSmartParkingBoy extends Boy{

    public SuperSmartParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    @Override
    public Ticket park(Car car) {
        Optional<ParkingLot> max = parkingLots.stream().
                max((parkingLot1, parkingLot2) ->
                        Float.compare(parkingLot1.getAvailablePositionRate(), parkingLot2.getAvailablePositionRate()));
        if (max.isPresent()) {
            return max.get().park(car);
        }
        throw new NoPositionException();
    }

    @Override
    public Car fetch(Ticket ticket) {
        Optional<ParkingLot> parkingLotOptional = parkingLots.stream().filter(parkingLot -> parkingLot.getTicketCarMap().containsKey(ticket)).findAny();
        if (parkingLotOptional.isPresent()) {
            return parkingLotOptional.get().fetch(ticket);
        }
        throw new UnrecognizedTicketException();
    }
}
