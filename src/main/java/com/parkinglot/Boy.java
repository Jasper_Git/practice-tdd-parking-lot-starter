package com.parkinglot;

import java.util.List;

/**
 * @program: practice-tdd-parking-lot-starter
 * @author: yoki
 * @create: 2023-07-16 14:38
 */
public abstract class Boy {
    protected List<ParkingLot> parkingLots;

    abstract Ticket park(Car car);

    abstract Car fetch(Ticket ticket);
}
