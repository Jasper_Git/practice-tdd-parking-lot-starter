package com.parkinglot;

public class NoPositionException extends RuntimeException{
    public NoPositionException() {
        super("No available position");
    }
}
