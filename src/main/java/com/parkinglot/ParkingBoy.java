package com.parkinglot;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public class ParkingBoy extends Boy {

    public ParkingBoy() {
    }

    public ParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    @Override
    protected Ticket park(Car car) {
        Optional<ParkingLot> first = parkingLots.stream().filter(ParkingLot::isNotFull).findFirst();
        if (first.isPresent()) {
            return first.get().park(car);
        }
        throw new NoPositionException();
    }

    @Override
    protected Car fetch(Ticket ticket) {
        Optional<ParkingLot> parkingLotOptional = parkingLots.stream().filter(parkingLot -> parkingLot.getTicketCarMap().containsKey(ticket)).findAny();
        if (parkingLotOptional.isPresent()) {
            return parkingLotOptional.get().fetch(ticket);
        }
        throw new UnrecognizedTicketException();
    }
}
