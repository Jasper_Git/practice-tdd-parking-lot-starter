## Objective

- Today, we have further exercises on Test-Drive-Development and object-orientation mainly through some case studies, and these stories are constantly progressing, which also corresponds to the changing process of ordinary business development. In addition, I learned what command mode is and the problems it solves.

## Reflective

- Through the continuous progression of stories, I have a deeper understanding of object orientation and TDD that I learned in the previous two days.
- Eliminating fear and uncertainty is an important reason to implement TDD, which allows me to write code more confidently and more willing to communicate.

## Interpretive

- TDD is a kind of gray-box testing, unlike traditional feature-based white-box testing, which focuses on testing external interfaces to make the program design easier to call.

## Decisional

- I need to use the assertion technique in the test.

## problem

- When writing implementation code for new test cases, I need to be very careful to write them to avoid failing tests that have already passed.